package main;

import main.beans.User;
import main.repo.GitHubUserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.context.annotation.SessionScope;


@SpringBootApplication
public class Application {

    public GitHubUserRepository repository;

    /** we declare a bean to be created by Spring in the app scope */
    @Bean
    @ApplicationScope
    public GitHubUserRepository gitHubUserRepository() { return repository; }

    /** we declare a bean to be created by Spring in each user session */
    @Bean
    @SessionScope
    public User userBean() { return new User(); }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
