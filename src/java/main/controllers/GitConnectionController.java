package main.controllers;

import main.repo.GitHubUser;
import main.repo.GitHubUserRepository;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.annotation.Resource;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Controller responsible for communication with GitHub.
 * Looking for a user and extracting the returning JSON
 */
@Controller
public class GitConnectionController {

    /** inject the gitHubUserRepository repo bean */
    @Resource(name = "gitHubUserRepository") private GitHubUserRepository repository;
    public GitHubUserRepository getRepo() { return repository; }

    /**
     * Looking for a user on GitHub
     * @param githubLogin login user
     * @param model
     * @return "redirect:/search";
     * @throws IOException
     */
    @GetMapping("/searchGitUser")
    public String searchGitUser(@ModelAttribute("githubLogin") String githubLogin,
                                Model model) throws IOException {

        GitHubUser user = getRepo().findByLogin(githubLogin);

        if (user != null) {

            try {
                JSONObject json = this.connectGitUub(githubLogin);
                String html_url = (String) json.get("html_url");
                int followers = (int) json.get("followers");

                user.setLink(new URL(html_url));
                user.setFollowers(followers);
                model.addAttribute("followers", followers);
                model.addAttribute("login", user.getLogin());

                return "search_details";

            } catch (Exception FileNotFoundException){
                throw new FileNotFoundException("Invalid user name:" + githubLogin);
            }
        }
        else return "redirect:/search";

    }

    /**
     * Extracting the returning JSON.
     * Read json from url use url.openStream() and read contents to a string.
     * @param githubLogin
     * @return JSONObject
     * @throws IOException
     * @throws ParseException
     */
    private JSONObject connectGitUub( String githubLogin) throws IOException, ParseException {

        URL oracle = new URL("https://api.github.com/users/" + githubLogin);
        BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));
        String inputLine;
        String toJson = "";
        while ((inputLine = in.readLine()) != null)
            toJson += inputLine;
        in.close();

        JSONParser parser = new JSONParser();
         return (JSONObject) parser.parse(toJson);
    }

}
