package main.controllers;
import main.repo.GitHubUserRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;

/**
 * Controller is responsible for displaying the 10 popular
 * searches in descending order stored in the database
 */
@Controller
public class HistoryController {

    /** inject the gitHubUserRepository repo bean */
    @Resource(name = "gitHubUserRepository") private GitHubUserRepository repository;
    public GitHubUserRepository getRepo() { return repository; }

    /**
     * SQL query to find popular searches
     * @param model
     * @return "history"
     */
    @GetMapping("/history")
    public String getHistory(Model model) {
        model.addAttribute("searches", getRepo().findFirst10ByOrderBySearchCounterDesc());
        return "history";
    }

}
