package main.controllers;

import main.repo.GitHubUser;
import main.repo.GitHubUserRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;

/**
 * Controller is responsible for search requests in Git
 */
@Controller
public class SearchController {

    /** inject the gitHubUserRepository repo bean */
    @Resource(name = "gitHubUserRepository") private GitHubUserRepository repository;
    public GitHubUserRepository getRepo() { return repository; }

    /**
     * add a new GitHubUser attribute to the model
     * @param model
     * @return search page
     */
    @GetMapping("/search")
    public String searchForm(Model model) {
        model.addAttribute("searchOnGitHub", new GitHubUser());
        return "search";
    }

    /**
     * add search to gitHubUserRepository according to the user search
     * @param gitHubUser
     * @param result
     * @param redirectAttributes
     * @return redirect:/searchGitUser
     */
    @PostMapping("/addSearch")
    public String addSearch(@ModelAttribute GitHubUser gitHubUser, BindingResult result,
                            final RedirectAttributes redirectAttributes) {

        if (result.hasErrors())
            return "redirect:/search";

        if (gitHubUser.getLogin() != "") {
            getRepo().save(gitHubUser);
            GitHubUser user = getRepo().findByLogin(gitHubUser.getLogin());
            user.addSearchOnGit();
            redirectAttributes.addFlashAttribute("githubLogin", gitHubUser.getLogin());
            return "redirect:/searchGitUser";
        }

        else return "redirect:/search";
    }

    /**
     * delete all history on gitHubUser database
     * @param model
     * @return redirect:/history
     */
    @GetMapping("/deleteAll")
    public String deleteSearches(Model model) {

        getRepo().deleteAll();
        model.addAttribute("searches", getRepo().findAll());
        return "redirect:/history";
    }

    /**
     * sign-out to home page
     * @return redirect:/
     */
    @GetMapping("/sign-out")
    public String logout() { return "redirect:/"; }

}
