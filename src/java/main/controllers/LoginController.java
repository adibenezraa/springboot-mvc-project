package main.controllers;

import main.beans.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * Controller responsible for the user login logic to the application
 */
@Controller
public class LoginController {

    /** inject the userBean */
    @Resource(name = "userBean") private User user;

    /** inject a property from the application.properties file */
    @Value("${username}") private String userName;
    @Value("${password}") private String password;

    /**
     * main page
     * @param model
     * @return login page
     */
    @GetMapping("/")
    public String main(Model model) {
        model.addAttribute("user", user);
        return "login";
    }

    /**
     * post http request to login page
     * @param user application user
     * @return If successful returns redirect:/search and if not returns the login page
     */
    @PostMapping("/login")
    public String postLogin(@Valid User user, BindingResult result) {

        if (result.hasErrors())
            return "login";

        this.user.setPassword(user.getPassword());
        this.user.setUserName(user.getUserName());

        if (this.user.getPassword().compareTo(this.password) == 0 &&
                this.user.getUserName().compareTo(this.userName) == 0) {
            this.user.setConnect(true);
            return "redirect:/search";
        }
        return "login";
    }
    /* get http request to login page */
    @GetMapping("/login")
    public String login() {
        return "redirect:/";
    }
}
