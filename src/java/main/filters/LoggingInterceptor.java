package main.filters;

import main.beans.User;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * this class intercepts all requests
 * block access to all pages as long as the user has not logged
 * in or if his session has already expired
 * */
public class LoggingInterceptor implements HandlerInterceptor {

    @Resource(name = "userBean")  private User user;

    public LoggingInterceptor(User user) { this.user = user; }

    /*
    * PreHandle
    * block access to all pages as long as the user has not logged
    * in or if his session has already expired
    * */
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {

        HttpSession session = request.getSession(false);

        if (session == null) {
            System.out.println("Unable to find session. Creating a new session");
            request.getSession(true);
        }

        if (user.isConnect())
            response.sendRedirect(request.getServletPath());

        else response.sendRedirect("login");

        return true;
    }


}

