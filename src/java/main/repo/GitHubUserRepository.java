package main.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * GitHubUserRepository
 */
public interface GitHubUserRepository extends JpaRepository<GitHubUser, Long> {
    /**
     *
     * @param userName
     * @return userName Login
     */
    GitHubUser findByLogin(String userName);

    /**
     *
     * @return findFirst10ByOrderBySearchCounterDesc
     */
    List<GitHubUser> findFirst10ByOrderBySearchCounterDesc();
}
