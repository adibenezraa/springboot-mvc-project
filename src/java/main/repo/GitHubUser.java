package main.repo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.net.URL;

/**
 * The program's database stores information about Github's user searches */
@Entity
public class GitHubUser {

    @Id
    @NotNull
    @NotBlank(message = "user login is mandatory")
    private String login;

    private URL link;
    private int followers;
    private int searchCounter = 0;

    public GitHubUser() {}

    public GitHubUser(String userName, String login, URL link, int followers) {
        this.login = login;
        this.link = link;
        this.followers = followers;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public void setLink(URL link) {
        this.link = link;
    }

    public URL getLink() {
        return this.link;
    }

    public void setSearchCounter(int searchCounter){
        this.searchCounter = searchCounter;
    }

    public int getSearchCounter(){
        return this.searchCounter;
    }

    public void setFollowers(int followers ) {this.followers = followers; }

    public int getFollowers () {return this.followers; }

    public void addSearchOnGit() {this.searchCounter +=1;}

}

