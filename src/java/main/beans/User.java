package main.beans;

import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * this is a bean class instantiated in session.
 * contains user name, password and boolean flag:isConnect,
 * A flag indicating whether the user is logged in to the app or not
 */

@Component
public class User implements Serializable{

    @NotBlank(message = "Name is mandatory") private String userName;

    @NotBlank(message = "Password is mandatory") private String password;

    private boolean isConnect = false;

    public User() {}

    /**
     * Constructor
     * @param userName
     * @param password
     */
    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public void setConnect(boolean connect) { isConnect = connect; }

    public boolean isConnect(){ return isConnect; }

    public void setUserName(String userName) { this.userName = userName; }

    public void setPassword(String password) { this.password = password; }

    public String getUserName() { return this.userName; }

    public String getPassword() { return this.password; }

    @Override
    public String toString() {
        return "User{"  +  ",username=" + userName + "isConnect=" + isConnect + '}';
    }

}
